package com.example.kalkulator2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView tvHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tvHasil = (TextView) findViewById(R.id.tvHasil2);

        Intent in = getIntent();
        Bundle b = in.getExtras();
        String s = b.getString(MainActivity.hasil);

        tvHasil.setText(s);
    }
}