package com.example.kalkulator2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9,
            btTambah, btKurang, btKali, btBagi,btHasil, btClear;
    TextView tv1, tv2, tv3, tvHasil;
    int val1 = 0, val2 = 0;
    boolean cekTambah, cekKurang, cekKali, cekBagi;
    public static final String hasil = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tvHasil = (TextView) findViewById(R.id.tvHasil);
        bt1 = (Button) findViewById(R.id.bt1);
        bt1.setOnClickListener(this);
        bt2 = (Button) findViewById(R.id.bt2);
        bt2.setOnClickListener(this);
        bt3 = (Button) findViewById(R.id.bt3);
        bt3.setOnClickListener(this);
        bt4 = (Button) findViewById(R.id.bt4);
        bt4.setOnClickListener(this);
        bt5 = (Button) findViewById(R.id.bt5);
        bt5.setOnClickListener(this);
        bt6 = (Button) findViewById(R.id.bt6);
        bt6.setOnClickListener(this);
        bt7 = (Button) findViewById(R.id.bt7);
        bt7.setOnClickListener(this);
        bt8 = (Button) findViewById(R.id.bt8);
        bt8.setOnClickListener(this);
        bt9 = (Button) findViewById(R.id.bt9);
        bt9.setOnClickListener(this);
        bt0 = (Button) findViewById(R.id.bt0);
        bt0.setOnClickListener(this);
        btTambah = (Button) findViewById(R.id.btTambah);
        btTambah.setOnClickListener(this);
        btKurang = (Button) findViewById(R.id.btKurang);
        btKurang.setOnClickListener(this);
        btKali = (Button) findViewById(R.id.btKali);
        btKali.setOnClickListener(this);
        btBagi = (Button) findViewById(R.id.btBagi);
        btBagi.setOnClickListener(this);
        btHasil = (Button) findViewById(R.id.btHasil);
        btHasil.setOnClickListener(this);
        btClear = (Button) findViewById(R.id.btClear);
        btClear.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.bt0) {
            tv2.setText(tv2.getText() + "0");
            tv3.setText(tv3.getText() + "0");
        } else if(view.getId() == R.id.bt1) {
            tv2.setText(tv2.getText() + "1");
            tv3.setText(tv3.getText() + "1");
        } else if(view.getId() == R.id.bt2) {
            tv2.setText(tv2.getText() + "2");
            tv3.setText(tv3.getText() + "2");
        } else if(view.getId() == R.id.bt3) {
            tv2.setText(tv2.getText() + "3");
            tv3.setText(tv3.getText() + "3");
        } else if(view.getId() == R.id.bt4) {
            tv2.setText(tv2.getText() + "4");
            tv3.setText(tv3.getText() + "4");
        } else if(view.getId() == R.id.bt5) {
            tv2.setText(tv2.getText() + "5");
            tv3.setText(tv3.getText() + "5");
        } else if(view.getId() == R.id.bt6) {
            tv2.setText(tv2.getText() + "6");
            tv3.setText(tv3.getText() + "6");
        } else if(view.getId() == R.id.bt7) {
            tv2.setText(tv2.getText() + "7");
            tv3.setText(tv3.getText() + "7");
        } else if(view.getId() == R.id.bt8) {
            tv2.setText(tv2.getText() + "8");
            tv3.setText(tv3.getText() + "8");
        } else if(view.getId() == R.id.bt9) {
            tv2.setText(tv2.getText() + "9");
            tv3.setText(tv3.getText() + "9");
        } else if(view.getId() == R.id.btTambah) {
            if(tv2 == null) {
                tv2.setText("");
            } else {
                val1 = Integer.parseInt(tv2.getText() + "");
                cekTambah = true;
                tv2.setText(null);
                tv3.setText(tv3.getText() + " + ");
            }
        } else if(view.getId() == R.id.btKurang) {
            if(tv2 == null) {
                tv2.setText("");
            } else {
                val1 = Integer.parseInt(tv2.getText() + "");
                cekKurang = true;
                tv2.setText(null);
                tv3.setText(tv3.getText() + " - ");
            }
        } else if(view.getId() == R.id.btKali) {
            if(tv2 == null) {
                tv2.setText("");
            } else {
                val1 = Integer.parseInt(tv2.getText() + "");
                cekKali = true;
                tv2.setText(null);
                tv3.setText(tv3.getText() + " * ");
            }
        } else if(view.getId() == R.id.btBagi) {
            if(tv2 == null) {
                tv2.setText("");
            } else {
                val1 = Integer.parseInt(tv2.getText() + "");
                cekBagi = true;
                tv2.setText(null);
                tv3.setText(tv3.getText() + " / ");
            }
        } else if(view.getId() == R.id.btHasil) {
            val2 = Integer.parseInt(tv2.getText() + "");
            tv2.setText(null);
            cekHasil();
            Intent in = new Intent(this, Activity2.class);
            Bundle b = new Bundle();
            b.putString(hasil, tv3.getText().toString() + " = " + tvHasil.getText().toString());
            in.putExtras(b);
            startActivity(in);
        } else if(view.getId() == R.id.btClear) {
            tv2.setText(null);
            tv3.setText(null);
            tvHasil.setText(null);
        }
    }

    private void cekHasil() {
        if(cekTambah) {
            tvHasil.setText(val1 + val2 + "");
            cekTambah = false;
        }
        if(cekKurang) {
            tvHasil.setText(val1 - val2 + "");
            cekKurang = false;
        }
        if(cekKali) {
            tvHasil.setText(val1 * val2 + "");
            cekKali = false;
        }
        if(cekBagi) {
            tvHasil.setText(val1 / val2 + "");
            cekBagi = false;
        }
    }
}